import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { Coding } from '@his-viewmodel/double-list/src/app/doubleset.interface';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'his-pick-list',
  standalone: true,
  imports: [CommonModule,DialogModule,TableModule,ButtonModule,InputTextModule],
  templateUrl: './pick-list.component.html',
  styleUrls: ['./pick-list.component.scss']
})
export class PickListComponent {

  @Input() value!:Coding[];
  @Input() pick!:Coding;

  @Output() changeVisible = new EventEmitter<boolean>();
  @Output() pickChange = new EventEmitter;


}
