import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { Coding } from '@his-viewmodel/double-list/src/app/doubleset.interface';
import { DialogModule } from 'primeng/dialog';
import { PickListComponent } from './pick-list/pick-list.component';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet,ButtonModule,DialogModule,PickListComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  visible: boolean = false;
  title = "請輸入頻率"
  value:Coding[] = [];
  pick!:Coding;


  showDialog(){
    this.visible = !this.visible;
  }

  onChangeVisible(visible:boolean){
    this.visible = visible;
  }

  ngOnInit() {
    this.value = [
      {
        code: "BID",
        display: "一日兩次"
      },
      {
        code: "BID&HS",
        display: "早晚飯後及睡前"
      },
      {
        code: "BIW",
        display: "每週兩次"
      },
      {
        code: "CM",
        display: "翌日"
      },
      {
        code: "CM&HS",
        display: "翌日及睡前"
      },
      {
        code: "HS",
        display: "睡前"
      },
      {
        code: "Q12H",
        display: "每12小時一次"
      },
      {
        code: "Q1H",
        display: "每小時一次"
      },
      {
        code: "Q2H",
        display: "每2小時一次"
      },
      {
        code: "Q2W",
        display: "兩週一次"
      },
    ]
  }
}
